import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  // STORE
  {
    path: '',
    loadChildren: './store/init/store-init.module#StoreInitModule',
    pathMatch: 'full'
  },
  {
    path: 'game/:id',
    loadChildren: './store/game/store-game.module#StoreGameModule'
  },
  {
    path: 'instructions/:id',
    loadChildren: './store/instructions/store-instructions.module#StoreInstructionsModule'
  },
  {
    path: 'selection/:id',
    loadChildren: './store/selection/store-selection.module#StoreSelectionModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
