import { NgModule } from '@angular/core';

import { CollapseModule } from 'ngx-bootstrap/collapse';

import { CommonModule } from '@angular/common';

// Core components
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared';

@NgModule({
    declarations: [
      TopNavbarComponent,
    ],
    imports: [
      SharedModule,
      CollapseModule,
      RouterModule,
      CommonModule
    ],
    providers: [
    ],
    exports: [
      TopNavbarComponent,
    ]
  })
export class CoreModule { }
