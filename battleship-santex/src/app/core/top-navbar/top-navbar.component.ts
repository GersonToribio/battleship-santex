import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./styles/top-navbar.styles.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopNavbarComponent {
  navbarCollapsed = true;
  loggedInObservable: Observable<boolean>;

  constructor(public router: Router) {
  }
}
