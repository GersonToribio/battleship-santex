import { GameStoreGameComponent } from './components/store-game/store-game.component';
import { StoreGameResolver } from './resolvers/store-game.resolver';
import { ModalGameComponent } from './components/store-modal/store-modal.component';

import { StoreGameModule } from './store-game.module';
