import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CarouselModule } from 'ngx-bootstrap/carousel';

import { SharedModule } from '../../shared';
import { StoreSharedModule } from '../';

import { GameStoreGameComponent } from './components/store-game/store-game.component';
import { StoreGameResolver } from './resolvers/store-game.resolver';

import { ModalModule } from 'ngx-bootstrap';
import { ModalGameComponent } from './components/store-modal/store-modal.component';

export const gameRoutes = [
    {
      path: '',
      component: GameStoreGameComponent,
      resolve: {
          data: StoreGameResolver
      }
    }
];

@NgModule({
    declarations: [
        GameStoreGameComponent,
        ModalGameComponent
    ],
    imports: [
      RouterModule.forChild(gameRoutes),
      CarouselModule,
      CommonModule,
      SharedModule,
      StoreSharedModule,

      ModalModule.forRoot()
    ],
    providers: [
        StoreGameResolver
    ],
    entryComponents: [
        ModalGameComponent
    ]
})
export class StoreGameModule { }
