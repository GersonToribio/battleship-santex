import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class StoreGameResolver implements Resolve<any> {
    constructor() {}

    resolve(route: ActivatedRouteSnapshot) {
        const level = route.params['id'];

        return new Promise((resolve, reject) => {
            // tslint:disable-next-line:triple-equals
            if (level != -1) {
                resolve({id: level});
            }
        });
    }
}
