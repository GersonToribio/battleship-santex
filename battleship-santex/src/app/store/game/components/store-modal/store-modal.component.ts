import { Component, ChangeDetectorRef, OnInit, ViewEncapsulation } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-modal-game',
    templateUrl: './store-modal.component.html',
    styleUrls: [
        './styles/store-modal.styles.scss'
    ],
    encapsulation: ViewEncapsulation.None
})

export class ModalGameComponent implements OnInit {
    public text = '';
    constructor(
        public bsModalRef: BsModalRef
    ) {}

    ngOnInit() {}
}
