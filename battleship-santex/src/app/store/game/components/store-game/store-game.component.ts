import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VERTICAL_POSITION, FIRST_SHIP, FOURTH_SHIP,
         HORIZONTAL_POSITION, SECOND_SHIP, THIRD_SHIP,
         FIRST_I, FIRST_J, MAX_I, MAX_J, MAX_SHIP,
         FIRST_SHIP_LONG, SECOND_SHIP_LONG, THIRD_SHIP_LONG, FOURTH_SHIP_LONG,
         POINTS_SELECTED,
         MISSED_SHOT,
         SHOOTED,
         GAME_EASY,
         GAME_MEDIUM,
         MAX_STEP_MEDIUM,
         GAME_HARD,
         MAX_STEP_HARD} from './store-game.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EASY_LEVEL } from 'src/app/store/init/components/store-init/store-init.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ModalGameComponent } from '../store-modal/store-modal.component';

@Component({
  selector: 'app-game-storegame',
  templateUrl: './store-game.component.html',
  styleUrls: ['./styles/store-game.styles.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GameStoreGameComponent implements OnInit {
  public user_board: number[][] = [];
  public pc_board: number[][] = [];

  // current pc index
  public index_pc_i = -1;
  public index_pc_j = -1;

  // Container for modal
  bsModalRef: BsModalRef;

  public currentTurn = 0;
  public currentUserTurn = 0;

  public level;

  FORM_FIELD = {};
  // Current step form
  public  form: FormGroup;

  public is_available = false;

  public user_board_points: number[][] = [];
  public pc_board_points: number[][] = [];

  constructor(private route: ActivatedRoute,
              private modalService: BsModalService,
              public fb: FormBuilder) {
      this.initializingForm();
      this.initializing(this.user_board);
      this.initializing(this.pc_board);

      this.initializing(this.user_board_points);
      this.initializing(this.pc_board_points);

      this.randomShips(this.user_board, this.user_board_points);
      this.randomShips(this.pc_board, this.pc_board_points);

      // After all initializations
      this.turn.setValue(this.currentTurn);

      /*console.log('this.user_board', this.user_board);
      console.log('this.pc_board', this.pc_board);
      let sum = 0;
      for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            if (this.user_board[i][j] !== 0) {
                sum += 1;
            }
        }
      }
      console.log('sum', sum);*/
  }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data && data !== null && data.id) {
        this.level = data.id;
      }
    });
  }

  private initializingForm() {
    this.FORM_FIELD = {
        turn: [null]
    };
    this.form = this.fb.group(this.FORM_FIELD);
    const obsTurn$ = this.turn.valueChanges.subscribe((value) => {
        if (value % 2 === 0) { // PC MOVES
            while (this.validatePCmoves()) {
                this.index_pc_i = -1;
                this.index_pc_j = -1;
            }

            if (this.user_board[this.index_pc_i][this.index_pc_j] === 0) { // Ocean
                this.user_board[this.index_pc_i][this.index_pc_j] = MISSED_SHOT;

                // tslint:disable-next-line:triple-equals
                if (this.level == EASY_LEVEL) {
                    this.currentTurn = this.currentTurn === 1 ? 0 : 1;
                    this.turn.setValue(this.currentTurn);
                } else {
                    this.currentTurn += 1;
                    this.turn.setValue(this.currentTurn);
                }
            } else if (this.user_board[this.index_pc_i][this.index_pc_j] <= 4) { // Ship
                this.user_board[this.index_pc_i][this.index_pc_j] = SHOOTED;

                // tslint:disable-next-line:triple-equals
                if (this.level == EASY_LEVEL) {
                    this.currentTurn = this.currentTurn === 1 ? 0 : 1;
                    this.turn.setValue(this.currentTurn);
                } else {
                    this.currentTurn += 1;
                    this.turn.setValue(this.currentTurn);
                }

                if (this.validateShipsAreSink(this.user_board)) {
                    this.showAlert('Cya! You lose. Don\'t give up!, Just go next :) !');
                } else {
                    this.showAlert('You have been attacked');
                }
            } else {
                // You click that place before
            }

            setTimeout(() => {
            }, 7000);
        }
    });
  }

  showAlert(text) {
    alert(text);
    /*this.bsModalRef = this.modalService.show(ModalGameComponent, Object.assign({}, { class: 'modal-md' }));
    this.bsModalRef.content.text = text;*/
  }

  validatePCmoves() {
    const index_i = Math.floor(Math.random() * 10);
    const index_j = Math.floor(Math.random() * 10);

    this.index_pc_i = index_i;
    this.index_pc_j = index_j;

    return this.user_board[this.index_pc_i][this.index_pc_j] > 4;
  }

  private initializing(matrix) {
    // initializing matrix
    for (let i = 0; i < 10; i++) {
        matrix[i] = [];
        for (let j = 0; j < 10; j++) {
            matrix[i][j] = 0;
        }
    }
  }

  private randomShips(matrix, selected) {
      const first_i = Math.floor(Math.random() * 10);
      const first_j = Math.floor(Math.random() * 10);
      this.backtrackingShips(matrix, selected, FIRST_SHIP, VERTICAL_POSITION, first_i, first_j, POINTS_SELECTED);
  }

  ramdomPosition() {
    const position = Math.floor(Math.random() * 2);
    if (position === 0) {
        return VERTICAL_POSITION;
    } else {
        return HORIZONTAL_POSITION;
    }
  }

  private lengthPosition(index_ship) {
      if (index_ship === FIRST_SHIP) {
        return FIRST_SHIP_LONG;
      } else if (index_ship === SECOND_SHIP) {
        return SECOND_SHIP_LONG;
      } else if (index_ship === THIRD_SHIP) {
        return THIRD_SHIP_LONG;
      } else if (index_ship === FOURTH_SHIP) {
        return FOURTH_SHIP_LONG;
      }
  }

  private paintCells(matrix, selected, length_ship, position, index_i, index_j) {
    for (let i = 0; i < length_ship; i++) {
        if (position === VERTICAL_POSITION) {
            matrix[index_i + i][index_j] = length_ship;
            selected[index_i + i][index_j] = length_ship;
        } else {
            selected[index_i][index_j + i] = length_ship;
            matrix[index_i][index_j + i] = length_ship;
        }
    }
  }

  private eraseCells(matrix, selected, length_ship, position, index_i, index_j) {
    for (let i = 0; i < length_ship; i++) {
        if (position === VERTICAL_POSITION) {
            matrix[index_i + i][index_j] = 0;
            selected[index_i + i][index_j] = 0;
        } else {
            selected[index_i][index_j + i] = 0;
            matrix[index_i][index_j + i] = 0;
        }
    }
  }

  sumAmount(index, axis, position, index_ship) {
    if (axis === 'i' && position === VERTICAL_POSITION) {
        const length_ship = this.lengthPosition(index_ship);
        return index + length_ship - 1;
    }
    if (axis === 'j' && position === HORIZONTAL_POSITION) {
        const length_ship = this.lengthPosition(index_ship);
        return index + length_ship - 1;
    }
    return index;
  }

  validateIntersections(matrix, selected, index_i, index_j, index_ship, position) {
    const length_ship = this.lengthPosition(index_ship);
    for (let  i = 0; i < length_ship; i++) {
        if (position === VERTICAL_POSITION) {
            if (matrix[index_i + i][index_j] !== 0) {
                return false;
            }
        } else {
            if (matrix[index_i][index_j + i] !== 0) {
                return false;
            }
        }
    }
    return true;
  }

  private backtrackingShips(matrix, selected, index_ship, position, index_i, index_j, points_selected) {
    if (index_ship >= MAX_SHIP) {
        this.is_available = true;
        return true;
    }
    if (points_selected > 100) {
        return false;
    }
    if (index_i >= MAX_I) {
        return false;
    }
    if (index_j >= MAX_J) {
        return false;
    }
    if (this.sumAmount(index_i, 'i', position, index_ship) >= MAX_I) {
        const changed_i = Math.floor(Math.random() * 10);
        const changed_j = Math.floor(Math.random() * 10);
        this.backtrackingShips(matrix, selected, index_ship, position, changed_i, changed_j, points_selected);
        return false;
    }
    if (this.sumAmount(index_j, 'j', position, index_ship) >= MAX_J) {
        const changed_new_i = Math.floor(Math.random() * 10);
        const changed_new_j = Math.floor(Math.random() * 10);
        this.backtrackingShips(matrix, selected, index_ship, position, changed_new_i, changed_new_j, points_selected);
        return false;
    }

    // if (selected[index_i][index_j] === 0) { // Not selected
    if (this.validateIntersections(matrix, selected, index_i, index_j, index_ship, position)) {
        const length_ship = this.lengthPosition(index_ship);
        this.paintCells(matrix, selected, length_ship, position, index_i, index_j);

        const new_i = Math.floor(Math.random() * 10);
        const new_j = Math.floor(Math.random() * 10);

        if (this.backtrackingShips(matrix, selected, index_ship + 1, this.ramdomPosition(), new_i, new_j, points_selected + length_ship)) {

        } else { // It is not allowed move
            if (!this.is_available) {
                this.eraseCells(matrix, selected, length_ship, position, index_i, index_j);
                return false;
            }
        }
    } else {
        const random_i = Math.floor(Math.random() * 10);
        const random_j = Math.floor(Math.random() * 10);
        this.backtrackingShips(matrix, selected, index_ship, position, random_i, random_j, points_selected);
    }
  }

  // CLASS SHIP
  public getFileShip(index_i, index_j) {
    if (this.user_board[index_i][index_j] === FIRST_SHIP + 1) {
        return '/assets/imgs/game/ship-1.png';
    } else if (this.user_board[index_i][index_j] === SECOND_SHIP + 1) {
        return '/assets/imgs/game/ship-2.png';
    } else if (this.user_board[index_i][index_j] === THIRD_SHIP + 1) {
        return '/assets/imgs/game/ship-3.png';
    } else if (this.user_board[index_i][index_j] === FOURTH_SHIP + 1) {
        return '/assets/imgs/game/ship-4.png';
    } else if (this.user_board[index_i][index_j] === MISSED_SHOT) {
        return '/assets/imgs/game/missed.png';
      } else if (this.user_board[index_i][index_j] === SHOOTED) {
        return '/assets/imgs/game/shooted.png';
      }
  }

  public getExplosions(index_i, index_j) {
      if (this.pc_board[index_i][index_j] === MISSED_SHOT) {
        return '/assets/imgs/game/missed.png';
      } else if (this.pc_board[index_i][index_j] === SHOOTED) {
        return '/assets/imgs/game/shooted.png';
      }
  }

  showImpact(index_i, index_j) {
      if (this.pc_board[index_i][index_j] === MISSED_SHOT ||
          this.pc_board[index_i][index_j] === SHOOTED) {
              return true;
          }
      return false;
  }

  public isAShip(index_i, index_j) {
    if (this.user_board[index_i][index_j] > 0 ) {
        return true;
    }
    return false;
  }

  public isUserTurn() {
    if (this.currentTurn % 2 === 0) {
        return true;
    }
    return false;
  }

  validateSteps() {
      // tslint:disable-next-line:triple-equals
      if (this.level == GAME_EASY) {
          return false;
      // tslint:disable-next-line:triple-equals
      } else if (this.level == GAME_MEDIUM && this.currentUserTurn < MAX_STEP_MEDIUM) {
          return false;
      // tslint:disable-next-line:triple-equals
      } else if (this.level == GAME_HARD && this.currentUserTurn < MAX_STEP_HARD) {
          return false;
      }
      return true;
  }

  showLevel() {
    // tslint:disable-next-line:triple-equals
    if (this.level == GAME_EASY) {
        return 'EASY';
    // tslint:disable-next-line:triple-equals
    } else if (this.level == GAME_MEDIUM) {
        return 'MEDIUM';
    // tslint:disable-next-line:triple-equals
    } else if (this.level == GAME_HARD) {
        return 'HARD';
    }
  }

  showTotalSteps() {
    // tslint:disable-next-line:triple-equals
    if (this.level == GAME_EASY) {
        return 'UNLIMITED';
    // tslint:disable-next-line:triple-equals
    } else if (this.level == GAME_MEDIUM) {
        return MAX_STEP_MEDIUM;
    // tslint:disable-next-line:triple-equals
    } else if (this.level == GAME_HARD) {
        return MAX_STEP_HARD;
    }
  }

  public validateShipsAreSink(matrix) {
    for (let i = 0; i < 10; i++) {
        for (let j = 0; j < 10; j++) {
            if (matrix[i][j] > 0 && matrix[i][j] < 5) {
                return false;
            }
        }
    }
    return true;
  }

  public shotAction(index_i, index_j) {

    if (this.validateSteps()) {
        this.showAlert('You Lose, nice try!');
        // Show Modal, You lose
        return false;
    }
    if (this.currentTurn % 2 !== 0) {

        if (this.pc_board[index_i][index_j] === 0) { // Ocean
            this.pc_board[index_i][index_j] = MISSED_SHOT;

            // tslint:disable-next-line:triple-equals
            if (this.level == EASY_LEVEL) {
                this.currentTurn = this.currentTurn === 1 ? 0 : 1;
                this.currentUserTurn = this.currentUserTurn === 1 ? 0 : 1;
                this.turn.setValue(this.currentTurn);
            } else {
                this.currentTurn += 1;
                this.currentUserTurn += 1;
                this.turn.setValue(this.currentTurn);
            }
        } else if (this.pc_board[index_i][index_j] <= 4) { // Ship
            this.pc_board[index_i][index_j] = SHOOTED;

            // tslint:disable-next-line:triple-equals
            if (this.level == EASY_LEVEL) {
                this.currentTurn = this.currentTurn === 1 ? 0 : 1;
                this.currentUserTurn = this.currentUserTurn === 1 ? 0 : 1;
                this.turn.setValue(this.currentTurn);
            } else {
                this.currentTurn += 1;
                this.currentUserTurn += 1;
                this.turn.setValue(this.currentTurn);
            }

            if (this.validateShipsAreSink(this.pc_board)) {
                this.showAlert('Dude! Your are amazing, you Win. That was easy, wasn\'t it?');
            } else {
                this.showAlert('You shooted a ship. Let\'s Go!');
            }

        } else {
            // You click that place before
        }
    }
  }

  // -- GETTER
  get turn() { return this.form.get('turn'); }

}
