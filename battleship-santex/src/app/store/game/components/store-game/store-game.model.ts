export const FIRST_SHIP = 0;
export const SECOND_SHIP = 1;
export const THIRD_SHIP = 2;
export const FOURTH_SHIP = 3;

export const FIRST_SHIP_LONG = 1;
export const SECOND_SHIP_LONG = 2;
export const THIRD_SHIP_LONG = 3;
export const FOURTH_SHIP_LONG = 4;

export const VERTICAL_POSITION = 'v';
export const HORIZONTAL_POSITION = 'h';

export const FIRST_I = 0;
export const FIRST_J = 0;

export const MAX_I = 10;
export const MAX_J = 10;
export const MAX_SHIP = 4;

export const POINTS_SELECTED = 0;

export const MISSED_SHOT = 8;
export const SHOOTED = 7;

export const GAME_EASY = 0;
export const GAME_MEDIUM = 1;
export const GAME_HARD = 2;

export const MAX_STEP_MEDIUM = 100;
export const MAX_STEP_HARD = 50;

