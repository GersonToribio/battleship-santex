import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-selection-storeselection',
  templateUrl: './store-selection.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SelectionStoreSelectionComponent {

  constructor(private route: ActivatedRoute) {
  }
}
