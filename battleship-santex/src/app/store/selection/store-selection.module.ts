import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CarouselModule } from 'ngx-bootstrap/carousel';

import { SharedModule } from '../../shared';
import { StoreSharedModule } from '../';

import { SelectionStoreSelectionComponent } from './components/store-selection/store-selection.component';

export const selectionRoutes = [
    {
      path: '',
      component: SelectionStoreSelectionComponent,
      resolve: {
      }
    }
];

@NgModule({
    declarations: [
        SelectionStoreSelectionComponent
    ],
    imports: [
      RouterModule.forChild(selectionRoutes),
      CarouselModule,
      CommonModule,
      SharedModule,
      StoreSharedModule
    ],
    providers: [
    ]
})
export class StoreSelectionModule { }
