import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NULL_LEVEL, EASY_LEVEL, HARD_LEVEL, MEDIUM_LEVEL } from './store-init.model';

@Component({
  selector: 'app-init-storeinit',
  templateUrl: './store-init.component.html',
  styleUrls: ['./styles/store-init.styles.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InitStoreInitComponent {
  public currentLevel = NULL_LEVEL;
  public TEASY_LEVEL = EASY_LEVEL;
  public TMEDIUM_LEVEL = MEDIUM_LEVEL;
  public THARD_LEVEL = HARD_LEVEL;

  constructor(
      private route: ActivatedRoute,
      public router: Router
      ) {
  }

  public choseLevel(level) {
    this.currentLevel = level;
  }

  public actionPlayNow() {
    this.router.navigate(['/instructions', this.currentLevel]);
  }
}
