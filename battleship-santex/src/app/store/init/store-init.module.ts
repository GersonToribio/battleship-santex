import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CarouselModule } from 'ngx-bootstrap/carousel';

import { SharedModule } from '../../shared';
import { StoreSharedModule } from '../';

import { InitStoreInitComponent } from './components/store-init/store-init.component';

export const initRoutes = [
    {
      path: '',
      component: InitStoreInitComponent,
      resolve: {
      }
    }
];

@NgModule({
    declarations: [
        InitStoreInitComponent
    ],
    imports: [
      RouterModule.forChild(initRoutes),
      CarouselModule,
      CommonModule,
      SharedModule,
      StoreSharedModule
    ],
    providers: [
    ]
})
export class StoreInitModule { }
