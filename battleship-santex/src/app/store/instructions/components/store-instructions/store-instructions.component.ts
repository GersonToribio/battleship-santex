import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-instructions-storeinstructions',
  templateUrl: './store-instructions.component.html',
  styleUrls: ['./styles/store-instructions.styles.scss'],
  encapsulation: ViewEncapsulation.None
})

export class InstructionsStoreInstructionsComponent implements OnInit {
  public level;

  constructor(
      private route: ActivatedRoute,
      public router: Router
      ) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      const data = routeData['data'];
      if (data && data !== null && data.id) {
        this.level = data.id;
      }
    });
  }

  public startGame() {
    this.router.navigate(['/game', this.level]);
  }
}
