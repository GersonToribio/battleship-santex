import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CarouselModule } from 'ngx-bootstrap/carousel';

import { SharedModule } from '../../shared';
import { StoreSharedModule } from '../';

import { InstructionsStoreInstructionsComponent } from './components/store-instructions/store-instructions.component';
import { StoreInstructionsResolver } from './resolvers/store-instructions.resolver';

export const instructionsRoutes = [
    {
      path: '',
      component: InstructionsStoreInstructionsComponent,
      resolve: {
          data: StoreInstructionsResolver
      }
    }
];

@NgModule({
    declarations: [
        InstructionsStoreInstructionsComponent
    ],
    imports: [
      RouterModule.forChild(instructionsRoutes),
      CarouselModule,
      CommonModule,
      SharedModule,
      StoreSharedModule
    ],
    providers: [
        StoreInstructionsResolver
    ]
})
export class StoreInstructionsModule { }
