import { InstructionsStoreInstructionsComponent } from './components/store-instructions/store-instructions.component';
import { StoreInstructionsResolver } from './resolvers/store-instructions.resolver';

import { StoreInstructionsModule } from './store-instructions.module';
