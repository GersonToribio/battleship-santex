**This is battleship santex**

**Description**

This platform was code in Angular 7. The algorithm that I used to find a position for the battleships was backtracking. There I put conditionals to not allow intersection of the random ships' position, breaker condition to finish the algorithm and evaluation for each loop excecution.

**Steps for installation**

You need to follow the steps to run the application

1. open a terminal in the file that has the project
2. Write 'npm install'
3. Then write 'npm run start'
4. Finally, go to http://localhost:4200, and you can pay the game. :) 

If you want more support, my e-mail gerson.toribio@pucp.pe

